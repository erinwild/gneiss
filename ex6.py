# Author: Erin Wild
# Email: ewild@uoguelph.ca (eewild@gmail.com)
# Affiliation: University of Guelph
# Supplementary code for research titled: 'An evolutionary approach to solving generalized Nash equilibrium problems with exclusively shared constraints'
# Submitted to: Journal of Heuristics
# Date: May 2017

import numpy as np

# riverbasin example
# 	P1: minimize a1*x+b*(x+y+z)-c1)*x, 0 <= x, 3.25*x + 1.25*y + 4.125*z <= 100, 2.2915*x + 1.5625*y + 2.8125*z <= 100
# 	P2: minimize a2*y+b*(x+y+z)-c2)*y, 0 <= y, 3.25*x + 1.25*y + 4.125*z <= 100, 2.2915*x + 1.5625*y + 2.8125*z <= 100
# 	P3: minimize a3*z+b*(x+y+z)-c3)*z, 0 <= z, 3.25*x + 1.25*y + 4.125*z <= 100, 2.2915*x + 1.5625*y + 2.8125*z <= 100
# 	sol: unknown

def payoffs(strat):
	"""Calculates all payoffs for a given strategy profile."""
	x = strat[0]
	y = strat[1]
	z = strat[2]
	
	## Defining payoffs and constraints from GNEP example
	a1 = 0.01
	a2 = 0.05
	a3 = 0.01
	b = 0.01
	c1 = 2.9
	c2 = 2.88
	c3 = 2.85
	
	payoffs = [ (a1*x+b*(x+y+z)-c1)*x,
				(a2*y+b*(x+y+z)-c2)*y,
				(a3*z+b*(x+y+z)-c3)*z ]
	constraints = [ 0 <= x, 0 <= y, 0 <= z, 3.25*x + 1.25*y + 4.125*z <= 100, 2.2915*x + 1.5625*y + 2.8125*z <= 100 ]
	
	if not all(constraints):
		for index, plyr_constr in enumerate(payoffs):
			payoffs[index] = infeasible
	return np.array(payoffs)

## Defining generators (vertices which contain feasible region: must be an n-parallelpiped)
generators = np.array([[0,0,0], [0,100/1.5625,0], [0,0,100/4.125], [0,100/1.5625,100/4.125], [100/3.25,0,0], [100/3.25,100/1.5625,0], [100/3.25,0,100/4.125], [100/3.25,100/1.5625,100/4.125]])

num_generators = len(generators) # number of generators
dim = len(generators[0]) # dimension of solution

rep_depth = 10 # representation depth
excl_depth = 3 # exclusion depth
alpha = 0.5 # averaging parameter
dec_var_dim = [1,1,1] # number of decision variables each player controls (within a complete strategy profile); the sum will be equal to dim


infeasible = 1000000000 # payoff for infeasible strategy profiles

## Defining algorithm parameters
pop_size = 15 # population size
rand_pop_size = 25 # random population size - doesn't evolve
runs = 500 # number of simulation restarts in order to collect solutions in dictionary
mevs = 1000 # number of mating events (generations)
t_size = 7 # tournament size
num_mut = 4 # number of mutations