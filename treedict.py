# Author: Erin Wild
# Email: ewild@uoguelph.ca (eewild@gmail.com)
# Affiliation: University of Guelph
# Supplementary code for research titled: 'An evolutionary approach to solving generalized Nash equilibrium problems with exclusively shared constraints'
# Submitted to: Journal of Heuristics
# Date: May 2017

import node.base as node # node 0.9.16 can be found at https://pypi.python.org/pypi/node

def save(treedict,save_str):
	"""Saves a string in dictionary tree."""
	if len(save_str) != 0:
		first_char = save_str[0]
		if first_char not in treedict:
			treedict[first_char] = node.Node()
			if len(save_str) != 1:
				save(treedict[first_char],save_str[1:])
		else:
			if len(save_str) != 1:
				save(treedict[first_char],save_str[1:])

def ask(treedict,ask_str):
	"""Returns true if string is in dictionary tree."""
	if len(ask_str) != 0:
		first_char = ask_str[0]
	else:
		return False
	for child in treedict:
		if child == first_char:
			if len(ask_str) == 1:
				if len(treedict[child]) == 0:
					return True
				else:
					break
			else:
				return ask(treedict[child],ask_str[1:])
	return False

def strings(treedict):
	"""Returns a list which contains the strings from an expanded dictionary tree."""
	array = []
	for child in treedict:
		array = array + strings(treedict[child])
	if array == []:
		return [treedict.name]
	else:
		for i in xrange(0,len(array)):
			array[i] = treedict.name + array[i]
	return array