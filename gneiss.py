# Author: Erin Wild
# Email: ewild@uoguelph.ca (eewild@gmail.com)
# Affiliation: University of Guelph
# Supplementary code for research titled: 'An evolutionary approach to solving generalized Nash equilibrium problems with exclusively shared constraints'
# Submitted to: Journal of Heuristics
# Date: May 2017

import numpy as np
import random
from datetime import datetime

import node.base as node # https://pypi.python.org/pypi/node
import treedict # treedict.py (tree-structured dictionary)

"""Import GNEP example which defines payoffs, constraints, generators, simulation parameters. ex1.py-ex8.py have been included."""
import ex1 as gnep # gnep definition (change 'ex1')
data_filename = 'ex1' # results will be saved using this filename


num_replicates = 1 # number of times to repeat the simulation

def nash_dom_k(strat1,strat2):
	"""Calculates Nash dominance score: k is number of players who would switch from strategy1 to strategy2."""
	k = 0
	orig_payoffs = gnep.payoffs(strat1)
	s = 0
	for i, plyr in enumerate(gnep.dec_var_dim):
		if strat1[s:s+plyr] != strat2[s:s+plyr]:
			switch_strat = np.hstack((strat1[:s],strat2[s:s+plyr],strat1[s+plyr:]))
			switch_payoffs = gnep.payoffs(switch_strat)
			if switch_payoffs[i] < orig_payoffs[i] or orig_payoffs[i] == gnep.infeasible:
				k += 1
		s += plyr
	return k

def sier_expand(agent):
	"""Sierpinski expansion: turns Sierpinski agent into Cartesian coordinate."""
	coord = np.array([0.0]*gnep.dim)
	for i in xrange(gnep.dim):
		coord[i] = gnep.generators[0][i]
	for j in xrange(gnep.rep_depth-1,-1,-1):
		for i in xrange(gnep.dim):
			coord[i] = (1-gnep.alpha)*coord[i] + gnep.alpha*gnep.generators[int(agent[j])][i]
	return coord

def eucl_dist(coord1,coord2):
	"""Returns Euclidean distance between two coordinates (for printing)."""
	return np.linalg.norm(coord1-coord2)

def convert(agent):
	"""Turns Sierpinksi agent into string."""
	return ''.join([str(int(bit)) for bit in agent])

def round_robin_tourn():
	"""Calculates average fitness from round robin tournament for each Sierpinski agent."""
	sum_k = np.zeros(gnep.pop_size)
	for i in xrange(gnep.pop_size):
		for j in xrange(i+1,gnep.pop_size):
			dist = eucl_dist(coords[i],coords[j])
			sum_k[i] += nash_dom_k(coords[i],coords[j])*dist
			sum_k[j] += nash_dom_k(coords[j],coords[i])*dist
		for r in xrange(gnep.rand_pop_size):
			sum_k[i] += nash_dom_k(coords[i],coords_r[r])*eucl_dist(coords[i],coords_r[r])
		fit[i] = float(sum_k[i])/(gnep.pop_size+gnep.rand_pop_size-1.0)
		if treedict.ask(saved,convert(pop[i])):
			fit[i] += 1 # penalize duplicate solution

for rep in xrange(1,num_replicates+1):
	startTime = datetime.now()
	print "rep %i starting..." % rep
	
	## Defining dictionary to save best agents
	saved = node.Node(name='')
	
	for run in xrange(gnep.runs):
		print "run %i starting..." % run
		
		## Initialize population
		pop = np.zeros((gnep.pop_size,gnep.rep_depth)) # population of Sierpinski strings
		rpop = np.zeros((gnep.rand_pop_size,gnep.rep_depth)) # random population of (non-evolving) Sierpinski strings
		coords = [[]]*gnep.pop_size # OPTMZ: expand strings only once and store
		coords_r = [[]]*gnep.rand_pop_size # OPTMZ: expand random strings only once and store
		fit = np.zeros(gnep.pop_size) # fitness value
		for i in xrange(gnep.pop_size):
			for j in xrange(gnep.rep_depth):
				pop[i][j] = random.randint(0,gnep.num_generators-1)
				fit[i] = gnep.infeasible
			coords[i] = sier_expand(pop[i])
		for i in xrange(gnep.rand_pop_size):
			for j in xrange(gnep.rep_depth):
				rpop[i][j] = random.randint(0,gnep.num_generators-1)
			coords_r[i] = sier_expand(rpop[i])
		
		## Round robin tournament to calculate average fitness
		round_robin_tourn()
		
		mev = -1
		while mev < gnep.mevs:
			mev += 1
			## Choose and sort the mating tournament participants
			dx = np.random.permutation(gnep.pop_size) # sorting index
			dx[:gnep.t_size] = dx[:gnep.t_size][fit[dx][:gnep.t_size].argsort()]
			
			## One-point crossover
			crssdex = random.randint(0,gnep.rep_depth-1)
			for i in xrange(crssdex):
				pop[dx[gnep.t_size-2]][i] = pop[dx[0]][i]
				pop[dx[gnep.t_size-1]][i] = pop[dx[1]][i]
			for i in xrange(crssdex,gnep.rep_depth):
				pop[dx[gnep.t_size-2]][i] = pop[dx[1]][i]
				pop[dx[gnep.t_size-1]][i] = pop[dx[0]][i]
			
			## Mutation
			for i in xrange(gnep.num_mut):
				pop[dx[gnep.t_size-1]][random.randint(0,gnep.rep_depth-1)] = random.randint(0,gnep.num_generators-1)
				pop[dx[gnep.t_size-2]][random.randint(0,gnep.rep_depth-1)] = random.randint(0,gnep.num_generators-1)
			
			## Update changed coordinates
			coords[dx[gnep.t_size-2]] = sier_expand(pop[dx[gnep.t_size-2]])
			coords[dx[gnep.t_size-1]] = sier_expand(pop[dx[gnep.t_size-1]])
			
			## Round robin tournament to re-calculate average fitness
			round_robin_tourn()
		
		## Saving perfect scores
		for i in xrange(gnep.pop_size):
			if fit[i] == 0.0:
				treedict.save(saved,convert(pop[i]))
				print coords[i], convert(pop[i])
		
	
	## Printing all saved solutions
	saved_np = treedict.strings(saved)
	f_sol = open(data_filename + '_' + str(rep) + '.txt', 'w')
	f_sol.write(' '.join([s for s in saved_np]) + '\n')
	saved_np = np.array(saved_np)
	
	## Checking saved solutions
	for i in xrange(gnep.rand_pop_size):
		for j in xrange(gnep.rep_depth):
			rpop[i][j] = random.randint(0,gnep.num_generators-1)
		coords_r[i] = sier_expand(rpop[i])
	sol_sum_k = np.zeros(len(saved_np))
	sol_fit = np.zeros(len(saved_np))
	for i in xrange(len(saved_np)):
		coord_i = sier_expand(saved_np[i])
		for j in xrange(i+1,len(saved_np)):
			coord_j = sier_expand(saved_np[j])
			dist = eucl_dist(coord_i,coord_j)
			sol_sum_k[i] += nash_dom_k(coord_i,coord_j)*dist
			sol_sum_k[j] += nash_dom_k(coord_j,coord_i)*dist
		for r in xrange(gnep.rand_pop_size):
			sol_sum_k[i] += nash_dom_k(coord_i,coords_r[r])*eucl_dist(coord_i,coords_r[r])
		sol_fit[i] = float(sol_sum_k[i])/(len(saved_np)+gnep.rand_pop_size-1)
	
	## Printing final scores for saved solutions
	for i in xrange(len(saved_np)):
		f_sol.write('%s %s %r\n' % (' '.join([str(bit).strip('[ ]') for bit in sier_expand(saved_np[i])]), convert(saved_np[i]), sol_fit[i]))
	
	print "...took " + str((datetime.now()-startTime).total_seconds()/60.0) + " minutes"
	f_sol.write(str((datetime.now()-startTime).total_seconds()/60.0) + ' minutes')
	f_sol.close()