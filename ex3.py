# Author: Erin Wild
# Email: ewild@uoguelph.ca (eewild@gmail.com)
# Affiliation: University of Guelph
# Supplementary code for research titled: 'An evolutionary approach to solving generalized Nash equilibrium problems with exclusively shared constraints'
# Submitted to: Journal of Heuristics
# Date: May 2017

import numpy as np

# Nabetani's second example
# 	P1: minimize x^2-xy-x, x>=0, x^2+y^2<=1
# 	P2: minimize y^2-1/2(xy)-2y, y>=0, x^2+y^2<=1
# 	sol: (t, sqrt(1-t^2)) for 0<=t<=4/5

def payoffs(strat):
	"""Calculates all payoffs for a given strategy profile."""
	x = strat[0]
	y = strat[1]
	
	## Defining payoffs and constraints from GNEP example
	payoffs = [ x**2-x*y-x,
				y**2-(1/2.0)*x*y-2*y ]
	constraints = [ [0 <= x, x <= 1, x**2+y**2 <= 1],
					[0 <= y, y <= 1, x**2+y**2 <= 1] ]
	
	for index, plyr_constr in enumerate(constraints):
		if not all(plyr_constr): # all constraints must be satisified
			payoffs[index] = infeasible
	return np.array(payoffs)

## Defining generators (vertices which contain feasible region: must be an n-parallelpiped)
generators = np.array([[0,0], [0,1], [1,0], [1,1]])

num_generators = len(generators) # number of generators
dim = len(generators[0]) # dimension of solution

rep_depth = 10 # representation depth
excl_depth = 3 # exclusion depth
alpha = 0.5 # averaging parameter
dec_var_dim = [1,1] # number of decision variables each player controls (within a complete strategy profile); the sum will be equal to dim


infeasible = 1000000000 # payoff for infeasible strategy profiles

## Defining algorithm parameters
pop_size = 15 # population size
rand_pop_size = 25 # random population size - doesn't evolve
runs = 300 # number of simulation restarts in order to collect solutions in dictionary
mevs = 1000 # number of mating events (generations)
t_size = 7 # tournament size
num_mut = 4 # number of mutations