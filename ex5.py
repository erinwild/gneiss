# Author: Erin Wild
# Email: ewild@uoguelph.ca (eewild@gmail.com)
# Affiliation: University of Guelph
# Supplementary code for research titled: 'An evolutionary approach to solving generalized Nash equilibrium problems with exclusively shared constraints'
# Submitted to: Journal of Heuristics
# Date: May 2017

import numpy as np

# Nabetani's third example
# 	P1: min x^2+xy+y^2+(x+y)z-25x-38y, s.t. x+2y-z<=14, 3x+2y+z<=30, x>=0, y>=0
# 	P2: min z^2+(x+y)z-25z, s.t. x+2y-z<=14, 3x+2y+z<=30, z>=0
# 	sol: (t, 11-t, 8-t) for 0<=t<=2 

def payoffs(strat):
	"""Calculates all payoffs for a given strategy profile."""
	x = strat[0]
	y = strat[1]
	z = strat[2]
	
	## Defining payoffs and constraints from GNEP example
	payoffs = [ (x**2)+x*y+(y**2)+(x+y)*z-25*x-38*y,
				(z**2)+(x+y)*z-25*z ]
	constraints = [ [0 <= x, 0 <= y, x+2*y-z <= 14, 3*x+2*y+z <= 30],
					[0 <= z, x+2*y-z <= 14, 3*x+2*y+z <= 30] ]
	
	for index, plyr_constr in enumerate(constraints):
		if not all(plyr_constr): # all constraints must be satisified
			payoffs[index] = infeasible
	return np.array(payoffs)

## Defining generators (vertices which contain feasible region: must be an n-parallelpiped)
generators = np.array([[0,0,0], [0,11,0], [0,0,30], [0,11,30], [10,0,0], [10,11,0], [10,0,30], [10,11,30]])

num_generators = len(generators) # number of generators
dim = len(generators[0]) # dimension of solution

rep_depth = 10 # representation depth
excl_depth = 3 # exclusion depth
alpha = 0.5 # averaging parameter
dec_var_dim = [2,1] # number of decision variables each player controls (within a complete strategy profile); the sum will be equal to dim


infeasible = 1000000000 # payoff for infeasible strategy profiles

## Defining algorithm parameters
pop_size = 15 # population size
rand_pop_size = 25 # random population size - doesn't evolve
runs = 500 # number of simulation restarts in order to collect solutions in dictionary
mevs = 1000 # number of mating events (generations)
t_size = 7 # tournament size
num_mut = 4 # number of mutations