# Author: Erin Wild
# Email: ewild@uoguelph.ca (eewild@gmail.com)
# Affiliation: University of Guelph
# Supplementary code for research titled: 'An evolutionary approach to solving generalized Nash equilibrium problems with exclusively shared constraints'
# Submitted to: Journal of Heuristics
# Date: May 2017

import numpy as np

# exclusively shared constraints example
# 	P1: minimize x^2-xy-x, x>=0, x^2+y^2<=1
# 	P2: minimize y^2-0.5xy-2y, y>=0, x^2+y^2<=1, z+y<=1
# 	P3: minimize (z-0.5)^2, z>=0, z+y<=1
# 	sol: unknown

def payoffs(strat):
	"""Calculates all payoffs for a given strategy profile."""
	x = strat[0]
	y = strat[1]
	z = strat[2]
	
	## Defining payoffs and constraints from GNEP example
	payoffs = [ x**2-x*y-x,
				y**2-0.5*x*y-2*y,
				(z-0.5)**2 ]
	constraints = [ [0 <= x, x**2+y**2<=1],
					[0 <= y, x**2+y**2<=1, z+y<=1],
					[0 <= z, z+y<=1] ]
	
	for index, plyr_constr in enumerate(constraints):
		if not all(plyr_constr): # all constraints must be satisified
			payoffs[index] = infeasible
	return np.array(payoffs)

## Defining generators (vertices which contain feasible region: must be an n-parallelpiped)
generators = np.array([[0,0,0], [0,1,0], [0,0,1], [0,1,1], [1,0,0], [1,1,0], [1,0,1], [1,1,1]])

num_generators = len(generators) # number of generators
dim = len(generators[0]) # dimension of solution

rep_depth = 10 # representation depth
excl_depth = 3 # exclusion depth
alpha = 0.5 # averaging parameter
dec_var_dim = [1,1,1] # number of decision variables each player controls (within a complete strategy profile); the sum will be equal to dim


infeasible = 1000000000 # payoff for infeasible strategy profiles

## Defining algorithm parameters
pop_size = 15 # population size
rand_pop_size = 25 # random population size - doesn't evolve
runs = 500 # number of simulation restarts in order to collect solutions in dictionary
mevs = 1000 # number of mating events (generations)
t_size = 7 # tournament size
num_mut = 4 # number of mutations